<?php

function fse_child_styles() {
	wp_enqueue_style( 'fse-child-style', get_stylesheet_uri() );
	wp_enqueue_style( 'wpforms', get_stylesheet_directory_uri() . '/wpforms.css');
}
add_action( 'wp_enqueue_scripts', 'fse_child_styles' );